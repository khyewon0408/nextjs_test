"use client"
import Link from 'next/link';
import { useParams ,useRouter } from 'next/navigation';

//아래 버튼 부분만 CSR처리
export function Controll() {
  const params = useParams();
 // console.log("useParams :: " ,  params);
 const id = params.id;
 const router =  useRouter();
  return (
    <ul>
      <li><Link href="/create">create</Link></li>
      {id? 
        <>
          <li><Link href={"/update/"+id}>update</Link></li>
          <li>
            <input type='button' value='delete' onClick={()=>{
              const options = {
                method: 'DELETE'
            }
              fetch(`${process.env.NEXT_PUBLIC_API_URL}topics/${id}`, options)
              .then(
                  resp=>resp.json()
              )
              .then(
                  result => {
                     router.push('/');
                     router.refresh();
                  }
              )
            }}/>
          </li>
        </> : null }
     
    </ul>
  );
}
