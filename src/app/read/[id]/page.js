export default async function Read(props){
    const reps = await fetch(process.env.NEXT_PUBLIC_API_URL+`topics/${props.params.id}`, {cache:'no-store'});
    const topic = await reps.json();
    return (
        <>
            <h2>title: {topic.title}</h2>
            content: {topic.body}
        </>
    )
}